/**
 * org.jrt.pe_nine is a list of operations to complete project euler problem nine
 */
package org.jrt.pe_nine;

/**
 * A Pythagorean triplet is a set of three natural numbers, a < b < c, for which,
 * a^2 + b^2 = c^2
 * For example, 32 + 42 = 9 + 16 = 25 = 52.
 * 
 * There exists exactly one Pythagorean triplet for which a + b + c = 1000.
 * Find the product abc.
 * 
 * @author jrtobac
 *
 */
public class Main {

	public static void main(String[] args) {
		int prod = 0;
		boolean keepLooping=true;
		for(int c=1; c<1000 && keepLooping; c++) {
			for(int b=1;b<c && keepLooping;b++) {
				for(int a =1; a<b && keepLooping; a++) {
					if(a+b+c == 1000) {
						if(((a*a) + (b*b)) == (c*c)) {
							prod = a*b*c;
							keepLooping=false;
						}
					}
				}
			}
		}
		System.out.println(prod);
	}

}
